<?php
/*
  Plugin Name: Menu of day
  Plugin URI: http://yann.linuxconsole.org
  Description: This plugin display picture send with <a href="https://play.google.com/store/apps/details?id=fr.menu.jour">Menu du jour</a> Android application
  Version: 1.0.1
  Author: Yann Le Doaré
  Author URI: https://bitbucket.org/yledoare/menuofdayphp
  License: CC-BY-ND
*/

add_action( 'admin_menu', 'menuofday_admin_menu' );
function menuofday_admin_menu() {
    add_options_page( 'MenuOfDay', 'MenuOfDay', 'manage_options', 'menuofday-plugin', 'menuofday_options_page' );
}

add_action( 'admin_init', 'menuofday_admin_init' );
function menuofday_admin_init() {
    register_setting( 'menuofday-settings-group', 'menuofday-user' );
    register_setting( 'menuofday-settings-group', 'menuofday-pass' );
    add_settings_section( 'section-one', 'Image upload access', 'section_one_callback', 'menuofday-plugin' );
    add_settings_field( 'field-one', 'User', 'field_one_callback', 'menuofday-plugin', 'section-one' );
    add_settings_field( 'field-two', 'Password', 'field_two_callback', 'menuofday-plugin', 'section-one' );
}

function section_one_callback() {
}

function field_two_callback() {
    $setting = esc_attr( get_option( 'menuofday-pass' ) );
    echo "<input type='text' name='menuofday-pass' value='$setting' />";
}
function field_one_callback() {
    $setting = esc_attr( get_option( 'menuofday-user' ) );
    echo "<input type='text' name='menuofday-user' value='$setting' />";
}


function menuofday_options_page() {
    ?>
    <div class="wrap">
        <h2>MenuOfDay Options</h2>
        <form action="options.php" method="POST">
            <?php settings_fields( 'menuofday-settings-group' ); ?>
            <?php do_settings_sections( 'menuofday-plugin' ); ?>
            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}


function display_menuofday( $atts ){

 $menuofday_user = esc_attr( get_option( 'menuofday-user' ) );
 $menuofday_pass = esc_attr( get_option( 'menuofday-pass' ) );

$jour=date("d");
$mois=date("m");
$annee=date("Y");
//echo "$annee/$mois/$jour";
$safe_filename="menudujour.png";	
$target_path = "wp-content/uploads/menudujour/$annee/$mois/$jour/";
$rEFileTypes = "/^\.(png|jpeg){1}$/i"; 
if(($_POST["password"] == "$menuofday_pass") && ($_POST["user"] == "$menuofday_user"))
{
$fp = fopen('log.txt', 'a');
	if(!is_dir($target_path)) mkdir($target_path,0777,true);

//$safe_filename = preg_replace( array("/\s+/", "/[^-\.\w]+/"), array("_", ""), trim($_FILES['uploadedfile']['name'])); 
$target_path = $target_path . $safe_filename; 
if(preg_match($rEFileTypes, strrchr($safe_filename, '.')))
{
		if(is_file($target_path)) { echo "Delete $target_path \n"; $res=unlink($target_path); echo "Return : $res \n";}
			if(move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
    $message="The file ".  basename( $_FILES['uploadedfile']['name']).  " has been uploaded \n";
			} else{
    $message="There was an error uploading the file, please try again!";
			}
    fwrite($fp,$message);
    echo "$message";

}
else
		echo "Error, your file $safe_filename is not accepted, please send file like jpg|jpeg|gif|png|doc|pdf";
fclose($fp);
}
else 
{
$jour=date("d");
$mois=date("m");
$annee=date("Y");
$safe_filename="menudujour.png";	
$target_path = "wp-content/uploads/menudujour/$annee/$mois/$jour/";
if(is_file($target_path.$safe_filename))
{
echo "
 <div style='width:90%; margin:auto;'>
        <h2 class='entry-title fontespeciale'>Notre menu du jour</h2>
        <img src='".$target_path.$safe_filename."' alt='Menu du jour'/>
  </div>
    ";
}
}
}

add_shortcode( 'menuofday', 'display_menuofday' );
//add_action ( 'wp_head', 'display_menuofday' );
?>
