# README #

This is server code for "Menu of day" Android application : https://play.google.com/store/apps/details?id=fr.menu.jour

### How do I get set up? ###

* Enable this plugin
* Activate menuofday shortcode on front page
* set up user and password into "settings" panel (wp-admin/options-general.php?page=menuofday-plugin)
* Add "[menuofday]" shortcode into your front page

<?php echo do_shortcode('[menuofday]') ?>

That's all !

### How do I send pictures to my Wordpress page ###

* Install Android application : https://play.google.com/store/apps/details?id=fr.menu.jour
* Enable URL/User/Password
* Take picture, send it 

