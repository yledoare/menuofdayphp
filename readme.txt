=== Plugin Name ===
Contributors: yledoare
Tags: picture, android
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Menuof day wordpress plugin is designed to receive picture from https://play.google.com/store/apps/details?id=fr.menu.jour android application

== Description ==

After installing plugin

*  set up user and password into "settings" panel (wp-admin/options-general.php?page=menuofday-plugin)
*  Add "[menuofday]" shortcode into your front page

== Manual Installation ==

1. Upload `menu-of-day.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Place `<?php do_action('display_menuofday'); ?>` in your templates

== Frequently Asked Questions ==

= Is if free ? =

Yes

= What about bugs ? =

See 

== Screenshots ==

Soon

== Changelog ==

1.0 First 